--[[
Copyright 2023 ekl

This file is part of Waybow.

Waybow is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Waybow is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with Waybow. If not, see <https://www.gnu.org/licenses/>. 
]] --
waybow = {}
waybow.gravity = vector.new(0, -9.8, 0)

local charging = {}

-- Wrapper for minetest.register_entity
-- Certain functions in definition may get wrapped by waybow.
function waybow.register_entity(name, definition)
  definition._waybow_age = 0
  if not definition._waybow_on_hit then
    definition._waybow_on_hit = waybow.on_hit
  end

  local on_step = definition.on_step
  function definition.on_step(self, dtime, moveresult)
    self._waybow_age = self._waybow_age + dtime
    local obj = self.object
    if not obj then
      return
    end

    local current_pos = obj:get_pos()
    obj:set_rotation(obj:get_velocity():dir_to_rotation())

    if self._waybow_age > 10 then
      self:_waybow_on_hit(nil)
      return
    end

    if not self._last_pos then -- Fallback to approximation if not available
      self._last_pos = current_pos:subtract(obj:get_velocity() * dtime)
    end
    -- Slight offset backwards in an attempt to reduce phasing
    local last_pos = self._last_pos:add(obj:get_velocity():normalize():multiply(-0.1))
    self._last_pos = current_pos

    local hit = Raycast(last_pos, current_pos, false, true):next()
    if hit then
      self:_waybow_on_hit(hit)
      return
    end

    if on_step then
      on_step(self, dtime, moveresult)
    end
  end

  function definition.on_activate(self, staticdata, dtime_s)
  end
  -- Default to disabling punch
  if not definition.on_punch then
    function definition.on_punch()
      return true
    end
  end
  minetest.register_entity(name, definition)
end

-- Default shoot function
function waybow.on_shoot(itemstack, user)
  local def = itemstack:get_definition()
  local speed = def._waybow_speed
  if speed == nil then
    speed = 0
  end
  local entity = minetest.add_entity(user:get_pos():offset(0, user:get_properties().eye_height, 0),
      def._waybow_projectile, nil)
  if entity then
    entity:set_velocity(user:get_velocity():add(user:get_look_dir():multiply(speed)))
    entity:set_rotation(user:get_look_dir():dir_to_rotation())
    entity:set_acceleration(waybow.gravity)
  end
  if itemstack._waybow_discharged then
    itemstack:set_name(itemstack._waybow_discharged)
    return itemstack
  end
end

-- Default projectile on hit
function waybow.on_hit(self, hit)
  local obj = self.object
  if obj then
    obj:remove()
  end
end

function waybow.on_full_charge(self, player)
  local def = self:get_definition()
  if def._waybow_next_charge then
    self:set_name(def._waybow_next_charge)
    return self
  end
end

-- This is a lower level wrapper with essentially the minimum of creature comfort.
-- A higher level wrapper will be implemented someday
-- Custom fields for definition:
-- _waybow_next_charge = Item for next charge level or unset if the bow will not charge any further
-- _waybow_discharged = Item for discharged version of bow
-- _waybow_charge_time = Time to charge to next level
function waybow.register_tool(name, definition)
  definition._waybow_charge_time = definition._waybow_charge_time or .5

  local on_use = definition.on_use
  definition.on_use = function(itemstack, user, pointed_thing)
    -- Todo: Add code to reset charge if somehow already charged
    charging[user] = 0 -- Start charging bow
    if on_use then
      return on_use(itemstack, user, pointed_thing)
    end
    local def = itemstack:get_definition()
    if def and def._waybow_discharged then
      itemstack:set_name(def._waybow_discharged)
      return itemstack
    end
  end

  if not definition._waybow_on_shoot then
    definition._waybow_on_shoot = waybow.on_shoot
  end

  if not definition._waybow_on_full_charge then
    definition._waybow_on_full_charge = waybow.on_full_charge
  end

  minetest.register_tool(name, definition)
end

minetest.register_globalstep(function(dtime)
  -- Remove invalid players from charging
  local connected = minetest.get_connected_players()
  local connectedSet = {}
  for _, object in pairs(connected) do
    connectedSet[object] = true
  end
  for player, _ in pairs(charging) do
    if not connectedSet[player] then
      charging[player] = nil
    end
  end

  for player, charge in pairs(charging) do
    local control = player:get_player_control()
    local wielditem = player:get_wielded_item()
    if control.dig then
      local def = wielditem:get_definition()
      -- Note: Charge time is currently always set so it is safe to use it to check if it is a bow
      if not (def and def._waybow_charge_time) then
        -- The bow is a lie
        charging[player] = nil
        -- Todo: Scrub the player inventory of any charged bows
      else
        -- Keep charging bow
        charge = charge + dtime
        if charge > def._waybow_charge_time then
          local newWield = def._waybow_on_full_charge(wielditem, player)
          if newWield then
            player:set_wielded_item(newWield)
          end
          charge = charge - def._waybow_charge_time
        end
        charging[player] = charge
      end
    else
      charging[player] = nil
      local def = wielditem:get_definition()
      local newItemStack = def._waybow_on_shoot(wielditem, player)
      if newItemStack then
        player:set_wielded_item(wielditem)
      elseif def._waybow_discharged then
        wielditem:set_name(def._waybow_discharged)
        player:set_wielded_item(wielditem)
      end
    end
  end
end)
