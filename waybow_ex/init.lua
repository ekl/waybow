--[[
Copyright 2023 ekl

This file is part of Waybow.

Waybow is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Waybow is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with Waybow. If not, see <https://www.gnu.org/licenses/>. 
]] --
local baseName = "waybow_ex:testbow" -- Name of the bow

-- Register all the charge levels of the bow
-- This will eventually get a nice wrapper to do this for you
for charge_level = 0, 4 do
  -- 
  local itemName = baseName
  if charge_level > 0 then
    itemName = ("%s_%d"):format(baseName, charge_level)
  end
  local image = itemName:gsub(":", "_") .. ".png"

  -- The actual definition.
  local def = {
    -- Note that these are all just standard minetest item definitions
    description = "Waybow Testbow",
    visual = "wielditem",
    wield_image = image,
    inventory_image = image,

    -- Configure waybow properties
    _waybow_discharged = baseName, -- Discharged version of bow
    _waybow_charge_time = 0.25, -- Seconds for each charge level
    _waybow_speed = (charge_level + 1) * 5, -- How fast the arrow flies
    _waybow_projectile = "waybow_ex:testbow_arrow" -- Tells the default shoot function what entity to spawn
  }

  -- Register the next charge level if it isn't fully charged
  if charge_level < 4 then
    def._waybow_next_charge = ("%s_%d"):format(baseName, charge_level + 1)
  end

  waybow.register_tool(itemName, def)
end

-- Our arrow, again note that this is essentially just an entity definition
waybow.register_entity("waybow_ex:testbow_arrow", {
  initial_properties = {
    visual = "mesh",
    mesh = "libwaybow_arrow.obj", -- A reference arrow object exists in libwaybow for convenience
    backface_culling = false,
    textures = {"waybow_ex_arrow.png"} -- 5x16 texture
  },
  _waybow_damage_multiplier = 0.5 -- Default damage is based on velocity*multiplier
})

-- Bonus: Machine gun bow
waybow.register_tool("waybow_ex:machinegun", {
  -- Note that these are all just standard minetest item definitions
  description = "Waybow Testbow Machinegun",
  visual = "wielditem",
  wield_image = "waybow_ex_testbow.png",
  inventory_image = "waybow_ex_testbow.png",

  -- Configure waybow properties
  _waybow_charge_time = 0.1, -- Time between shots
  _waybow_speed = 20,
  _waybow_on_full_charge = waybow.on_shoot,
  _waybow_projectile = "waybow_ex:testbow_arrow"
})
