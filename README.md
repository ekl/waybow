# Waybow

Unfinished attempt at a bow API, probably not going to be completed

Currently provides an API for bows (libwaybow), and a reference/example bow registration (waybow_ex)

This is extremely experimental. Expect breaking changes.

## Why another bow mod/lib? What does this do differently?

* Charging bows works in just one click and release
* Goal of simplicity. Bow and arrow registration is merely a wrapper over minetest registration functions.
* Modular structure allowing the user to override behaviour at will.
* Supports animated/multi-stage bow charging.

## Completion status
- [x] Lower level API (the wrappers)
- [ ] Higher level API to auto-register bow charge levels
- [x] Arrow shooting
- [ ] Default consumable ammunition system
- [ ] Handle switching bows mid-charge

## LibWaybow
Although the API table is called waybow, use libwaybow as the dependency for just the API, not waybow which is the modpack.

### API architecture
#### waybow.register_tool
Wrapper around minetest.register_tool for registering bows, see Minetest docs and waybow_ex/init.lua
#### waybow.register_entity
Wrapper around minetest.register_entity for registering arrows, see Minetest docs and waybow_ex/init.lua